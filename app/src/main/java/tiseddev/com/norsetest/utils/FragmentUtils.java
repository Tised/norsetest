package tiseddev.com.norsetest.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by dmytro_vodnik on 1/13/16.
 */

public class FragmentUtils {

    public static void openFragment(Fragment which, int where, String tag, Context context, Boolean isAddToBackStack){

        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(tag) == null) {

            FragmentTransaction t = fragmentManager.beginTransaction();

            t.replace(where, which, tag);
            if (isAddToBackStack)
                t.addToBackStack(null);
            t.commit();
        }
    }

    public static void closeFragment(Context context, String tag) {

        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();

        Fragment fragment = fragmentManager.findFragmentByTag(tag);

        if(fragment != null)
            fragmentManager.beginTransaction().remove(fragment).commit();
    }

}
