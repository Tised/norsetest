package tiseddev.com.norsetest.utils;

/**
 * Created by dmytro_vodnik on 2/13/16.
 */
public class Const {

    public static final String PREF_ACCOUNT_NAME = "accountName";
    public static final int REQUEST_AUTHORIZATION = 1001;
    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    public static final int REQUEST_ACCOUNT_PICKER = 1000;
}
