package tiseddev.com.norsetest.utils.DBUtils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import tiseddev.com.norsetest.dao.GmailThreadDAO;
import tiseddev.com.norsetest.dao.GmailThreadMessageDAO;
import tiseddev.com.norsetest.models.GmailThreadMessageModel;
import tiseddev.com.norsetest.models.GmailThreadModel;

/**
 * Created by tised on 8/12/15.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String TAG = "NORSETEST DATABASE";

    //имя файла базы данных который будет храниться в /data/data/APPNAME/DATABASE_NAME.db
    private static String DATABASE_NAME ="norsetest.db";

    //с каждым увеличением версии, при нахождении в устройстве БД с предыдущей версией будет выполнен метод onUpgrade();
    private static final int DATABASE_VERSION = 1;

    //ссылки на DAO соответсвующие сущностям, хранимым в БД
    private GmailThreadDAO gmailThreadDAO = null;
    private GmailThreadMessageDAO gmailThreadMessageDAO = null;

    Context context;

    public DatabaseHelper(Context context){
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    //Выполняется, когда файл с БД не найден на устройстве
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource){
        try {

            TableUtils.createTable(connectionSource, GmailThreadModel.class);
            TableUtils.createTable(connectionSource, GmailThreadMessageModel.class);
        }

        catch (SQLException e){

            Log.e(TAG, "error creating DB " + DATABASE_NAME);

            throw new RuntimeException(e);
        }
    }

    //Выполняется, когда БД имеет версию отличную от текущей
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer){

        switch (oldVer) {
            case 1:
                updateFromVersion1(db, connectionSource, oldVer, newVer);
                break;

            default:
                // no updates needed
                break;
        }
    }

    //выполняется при закрытии приложения
    @Override
    public void close(){
        super.close();
        gmailThreadDAO = null;
        gmailThreadMessageDAO = null;
    }

    private void updateFromVersion1(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer, int newVer) {

        Log.d(TAG, "updating DB from ver 1 ");

        onUpgrade(db, connectionSource, oldVer + 1, newVer);
    }

    public GmailThreadDAO getGmailThreadDAO() throws SQLException {

        if (gmailThreadDAO == null)
            gmailThreadDAO = new GmailThreadDAO(getConnectionSource(), GmailThreadModel.class);

        return gmailThreadDAO;
    }

    public GmailThreadMessageDAO getGmailThreadMessageDAO() throws SQLException {

        if (gmailThreadMessageDAO == null)
            gmailThreadMessageDAO = new GmailThreadMessageDAO(getConnectionSource(), GmailThreadMessageModel.class);

        return gmailThreadMessageDAO;
    }

    public void clearGmailThreadsTable() {

        try {
            TableUtils.clearTable(getConnectionSource(), GmailThreadModel.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void clearGmailThreadMessagesTable() {

        try {
            TableUtils.clearTable(getConnectionSource(), GmailThreadMessageModel.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
