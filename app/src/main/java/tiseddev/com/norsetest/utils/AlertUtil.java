package tiseddev.com.norsetest.utils;

import android.app.AlertDialog;
import android.content.Intent;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;

import tiseddev.com.norsetest.R;


/**
 * Created by tised on 7/31/15.
 */
public class AlertUtil {


    private static final String TAG = "ALERTS";

    public static void noInternetAlert (FragmentActivity calledActivity) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(calledActivity);

        alertDialogBuilder
                .setTitle(R.string.no_internet)
                .setMessage(R.string.enable_internet)
                .setCancelable(true)
                .setPositiveButton(R.string.ok, (dialog, id) -> {

                    Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                    calledActivity.startActivity(intent);
                })
                .setNegativeButton(R.string.no, ((dialog1, which) -> {

                    dialog1.dismiss();
                }));

        AlertDialog alert11 = alertDialogBuilder.create();
        alert11.show();
    }
}
