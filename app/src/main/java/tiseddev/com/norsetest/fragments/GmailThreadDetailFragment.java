package tiseddev.com.norsetest.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.ModifyMessageRequest;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import tiseddev.com.norsetest.R;
import tiseddev.com.norsetest.adapters.GmailThreadMessagesAdapter;
import tiseddev.com.norsetest.dividers.DividerItemDecoration;
import tiseddev.com.norsetest.interfaces.CredentialsInterface;
import tiseddev.com.norsetest.interfaces.LoadingInterface;
import tiseddev.com.norsetest.models.GmailThreadMessageModel;
import tiseddev.com.norsetest.models.GmailThreadModel;
import tiseddev.com.norsetest.utils.Const;
import tiseddev.com.norsetest.utils.DBUtils.HelperFactory;
import tiseddev.com.norsetest.utils.SharedPrefsUtils;

public class GmailThreadDetailFragment extends Fragment {

    private static final String ARG_GMAIL_THREAD = "param1";
    private static final String TAG = "THREAD DETAILS";
    LoadingInterface loadingInterface;
    private GmailThreadModel gmailThreadModel;
    GmailThreadMessagesAdapter gmailThreadMessagesAdapter;
    CredentialsInterface credentialsInterface;

    @Bind(R.id.messages_recycler)
    RecyclerView messagesRecycler;
    private Gmail mService;

    String[] removableLabels = {"UNREAD"};

    public GmailThreadDetailFragment() {
        // Required empty public constructor
    }

    public static GmailThreadDetailFragment newInstance(GmailThreadModel param1) {
        GmailThreadDetailFragment fragment = new GmailThreadDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_GMAIL_THREAD, param1);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            gmailThreadModel = (GmailThreadModel) getArguments().getSerializable(ARG_GMAIL_THREAD);

            Log.d(TAG, "showing messages from thread === " + gmailThreadModel);
        }

        gmailThreadMessagesAdapter = new GmailThreadMessagesAdapter(getActivity());

        String userId = SharedPrefsUtils.getStringPreference(getActivity(), Const.PREF_ACCOUNT_NAME);

        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        mService = new Gmail.Builder(
                transport, jsonFactory, credentialsInterface.getCurrentCredentials())
                .setApplicationName(getString(R.string.google_api_app_name))
                .build();
        try {
            List<GmailThreadMessageModel> gmailThreadMessageModels = HelperFactory.getHelper()
                    .getGmailThreadMessageDAO().getAllThreadMessages(gmailThreadModel.getThreadId());


            gmailThreadMessagesAdapter.addItems(gmailThreadMessageModels);

            new Thread(() -> {

                try {

                    for (GmailThreadMessageModel gmailThreadMessageModel : gmailThreadMessageModels) {

                        modifyMessage(mService, userId, gmailThreadMessageModel.getMessageId(), null, Arrays.asList(removableLabels));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_gmail_thread_detail, container, false);

        ButterKnife.bind(this, rootView);

        messagesRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.menu_divider_vertical));
        messagesRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        messagesRecycler.setAdapter(gmailThreadMessagesAdapter);

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.loadingInterface = (LoadingInterface) context;
        this.credentialsInterface = (CredentialsInterface) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private void modifyMessage(Gmail service, String userId, String messageId,
                               List<String> labelsToAdd, List<String> labelsToRemove) throws IOException {
        ModifyMessageRequest mods = new ModifyMessageRequest().setAddLabelIds(labelsToAdd)
                .setRemoveLabelIds(labelsToRemove);
        Message message = service.users().messages().modify(userId, messageId, mods).execute();

        Log.d(TAG, "modified === " + message);
    }
}
