package tiseddev.com.norsetest;

import android.accounts.AccountManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.gmail.GmailScopes;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import tiseddev.com.norsetest.adapters.GmailThreadsAdapter;
import tiseddev.com.norsetest.api_calls.GetThreadsTask;
import tiseddev.com.norsetest.dao.GmailThreadDAO;
import tiseddev.com.norsetest.dividers.DividerItemDecoration;
import tiseddev.com.norsetest.fragments.LoginFragment;
import tiseddev.com.norsetest.interfaces.CredentialsInterface;
import tiseddev.com.norsetest.interfaces.GmailThreadsInterface;
import tiseddev.com.norsetest.interfaces.LoadingInterface;
import tiseddev.com.norsetest.interfaces.LoginInterface;
import tiseddev.com.norsetest.models.GmailThreadModel;
import tiseddev.com.norsetest.utils.AlertUtil;
import tiseddev.com.norsetest.utils.Const;
import tiseddev.com.norsetest.utils.DBUtils.HelperFactory;
import tiseddev.com.norsetest.utils.DeviceStateUtils;
import tiseddev.com.norsetest.utils.FragmentUtils;
import tiseddev.com.norsetest.utils.SharedPrefsUtils;

public class MainActivity extends AppCompatActivity implements LoginInterface, CredentialsInterface,
        LoadingInterface, GmailThreadsInterface, OnBackStackChangedListener,
        NavigationView.OnNavigationItemSelectedListener {


    private static final String TAG = "LOGIN ACT";
    GoogleAccountCredential mCredential;
    Snackbar snackbar;

    @Bind(R.id.root_frame)
    FrameLayout root;

    @Bind(R.id.coordinator_root)
    CoordinatorLayout coordRoot;

    GmailThreadsAdapter adapter;

    @Bind(R.id.threads_recycler)
    RecyclerView recyclerView;

    TextView currentAccount;

    private static final String[] SCOPES = {GmailScopes.GMAIL_READONLY,
            GmailScopes.GMAIL_MODIFY, GmailScopes.GMAIL_LABELS};

    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;

    @Bind(R.id.swipe_threads)
    SwipeRefreshLayout swipeRefreshLayout;

    private ActionBarDrawerToggle mDrawerToggle;
    private NavigationView navigationView;
    private String pageToken;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);

        mLayoutManager = new LinearLayoutManager(this);
        adapter = new GmailThreadsAdapter(this);

        recyclerView.addItemDecoration(new DividerItemDecoration(this, R.drawable.menu_divider_vertical, false, true));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();

                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!swipeRefreshLayout.isRefreshing())
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            swipeRefreshLayout.setRefreshing(false);

                            loadGmailThreads();
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(() -> {

            HelperFactory.getHelper().clearGmailThreadsTable();
            HelperFactory.getHelper().clearGmailThreadMessagesTable();
            adapter.cleanList();
            setPageToken("");
            loadGmailThreads();
        });

        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff())
                .setSelectedAccountName(SharedPrefsUtils.getStringPreference(this, Const.PREF_ACCOUNT_NAME));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                0,
                0) {

            public void onDrawerClosed(View view) {
                syncActionBarArrowState();
            }

            public void onDrawerOpened(View drawerView) {
                mDrawerToggle.setDrawerIndicatorEnabled(true);
            }
        };

        drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        View headerView = navigationView.getHeaderView(0);

        currentAccount = (TextView) headerView.findViewById(R.id.acc_name);

        navigationView.setNavigationItemSelectedListener(this);

        toolbar.setNavigationOnClickListener(v -> {

            if ( getSupportFragmentManager().getBackStackEntryCount() == 0 ) {

                if (drawer.isDrawerOpen(Gravity.LEFT)) {
                    drawer.closeDrawer(Gravity.LEFT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
            } else {

                getSupportFragmentManager().popBackStack();
            }
        });

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportFragmentManager().addOnBackStackChangedListener(this);

        loadGmailThreads();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Const.REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    DeviceStateUtils.isGooglePlayServicesAvailable(this);
                }
                break;
            case Const.REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        mCredential.setSelectedAccountName(accountName);

                        SharedPrefsUtils.setStringPreference(this, Const.PREF_ACCOUNT_NAME, accountName);
                        FragmentUtils.closeFragment(this, "LOGIN");
                        loadGmailThreads();
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    Log.d(TAG, "Account unspecified.");
                }
                break;
            case Const.REQUEST_AUTHORIZATION:
                if (resultCode != RESULT_OK) {
                    chooseAccount();
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadGmailThreads() {
        if (mCredential.getSelectedAccountName() == null) {

            LoginFragment loginFragment = LoginFragment.newInstance();

            FragmentUtils.openFragment(loginFragment, R.id.root_frame, "LOGIN", this, true);
        } else {

            currentAccount.setText(mCredential.getSelectedAccountName());

            try {
                GmailThreadDAO gmailThreadDAO = HelperFactory.getHelper().getGmailThreadDAO();
                recyclerView.setAdapter(adapter);

                if (DeviceStateUtils.isDeviceOnline(this)) {

                    new GetThreadsTask(mCredential, this, this, this).execute(getCurrentPageToken());

                } else {

                    swipeRefreshLayout.setRefreshing(false);
                    Log.d(TAG, "No network connection available.");

                    List<GmailThreadModel> gmailThreadModels = null;

                        Log.d(TAG, "retrieving cahced data");
                        gmailThreadModels = gmailThreadDAO.getAllThreads();

                    if (gmailThreadModels != null) {

                        adapter.addItems(gmailThreadModels);
                    }

                    AlertUtil.noInternetAlert(this);
                }
            } catch (SQLException e) {
                Log.e(TAG, "error while retrieving cached data === " + Log.getStackTraceString(e));
            }
        }
    }

    /**
     * Starts an activity in Google Play Services so the user can pick an
     * account.
     */
    @Override
    public void chooseAccount() {

        startActivityForResult(mCredential.newChooseAccountIntent(), Const.REQUEST_ACCOUNT_PICKER);
    }

    @Override
    public GoogleAccountCredential getCurrentCredentials() {

        return mCredential;
    }

    @Override
    public void startLoading(String message) {

        snackbar = Snackbar.make(coordRoot, message, Snackbar.LENGTH_INDEFINITE);
        swipeRefreshLayout.setRefreshing(true);
        snackbar.show();
    }

    @Override
    public void stopLoading(String message) {

        snackbar.dismiss();
        snackbar = Snackbar.make(coordRoot, message, Snackbar.LENGTH_SHORT);
        snackbar.show();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void errorLoading(String errorMessage, View.OnClickListener errorListener) {

        swipeRefreshLayout.setRefreshing(false);
        Snackbar snackbar = Snackbar
                .make(coordRoot, errorMessage, Snackbar.LENGTH_INDEFINITE)
                .setAction("RETRY", errorListener);

        // Changing message text color
        snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    @Override
    public void addGmailThreads(List<GmailThreadModel> list) {

        adapter.addItems(list);
    }

    @Override
    public void addGmailThread(GmailThreadModel gmailThreadModel) {

        adapter.addItem(gmailThreadModel);
    }

    @Override
    public List<GmailThreadModel> getAllGmailThreads() {
        return null;
    }

    @Override
    public void setPageToken(String token) {

        this.pageToken = token;
    }

    @Override
    public String getCurrentPageToken() {

        return pageToken;
    }

    @Override
    public void onBackStackChanged() {

        syncActionBarArrowState();
    }

    private void syncActionBarArrowState() {
        int backStackEntryCount =
                getSupportFragmentManager().getBackStackEntryCount();
        mDrawerToggle.setDrawerIndicatorEnabled(backStackEntryCount == 0);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {

            case R.id.nav_change_acc:
                // Handle the camera action

                LoginFragment loginFragment = LoginFragment.newInstance();

                FragmentUtils.openFragment(loginFragment, R.id.root_frame, "LOGIN", this, true);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}