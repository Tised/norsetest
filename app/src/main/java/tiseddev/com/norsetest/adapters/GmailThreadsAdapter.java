package tiseddev.com.norsetest.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.andexert.library.RippleView;

import java.util.ArrayList;
import java.util.List;

import tiseddev.com.norsetest.R;
import tiseddev.com.norsetest.databinding.ItemGmailThreadBinding;
import tiseddev.com.norsetest.fragments.GmailThreadDetailFragment;
import tiseddev.com.norsetest.models.GmailThreadModel;
import tiseddev.com.norsetest.utils.FragmentUtils;

/**
 * Created by dmytro_vodnik on 2/13/16.
 */
public class GmailThreadsAdapter extends RecyclerView.Adapter<GmailThreadsAdapter.ViewHolder> {

    private static final String TAG = "GMAIL THREADS ADAP";
    private final Context context;
    LayoutInflater layoutInflater;
    ArrayList<GmailThreadModel> gmailThreadModels;

    public GmailThreadsAdapter(Context context) {

        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        gmailThreadModels = new ArrayList<>();
    }

    public void addItems(List<GmailThreadModel> list) {

        this.gmailThreadModels.clear();
        this.gmailThreadModels.addAll(list);
        notifyDataSetChanged();
    }

    public void cleanList() {

        this.gmailThreadModels.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ItemGmailThreadBinding binding;
        RippleView root;

        public ViewHolder(ItemGmailThreadBinding itemGmailThreadBinding) {
            super(itemGmailThreadBinding.getRoot());

            this.binding = itemGmailThreadBinding;
            this.root = (RippleView) itemGmailThreadBinding.getRoot();
        }
    }

    public void addItem(GmailThreadModel gmailThreadModel) {

        gmailThreadModels.add(gmailThreadModel);
        notifyItemInserted(gmailThreadModels.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        final ItemGmailThreadBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.item_gmail_thread, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        GmailThreadModel gmailThreadModel = getItem(position);

        holder.binding.setGmailThread(gmailThreadModel);
        holder.binding.setItemNum(String.valueOf(position));
        holder.root.setOnRippleCompleteListener(rippleView -> {

            Log.d(TAG, "show thread with id === " + gmailThreadModel.getThreadId() + " called");

            GmailThreadDetailFragment gmailThreadDetailFragment = GmailThreadDetailFragment.newInstance(gmailThreadModel);

            FragmentUtils.openFragment(gmailThreadDetailFragment, R.id.root_frame, "THREAD DETAILS", context,
                    true);
        });
    }

    public GmailThreadModel getItem(int position) {

        return gmailThreadModels.get(position);
    }

    @Override
    public int getItemCount() {

        return gmailThreadModels.size();
    }

    public void setPlaylistmodels(List<GmailThreadModel> list) {

        this.gmailThreadModels.addAll(list);
        notifyDataSetChanged();
    }
}