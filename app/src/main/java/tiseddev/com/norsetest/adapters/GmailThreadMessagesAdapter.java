package tiseddev.com.norsetest.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;
import java.util.List;

import tiseddev.com.norsetest.R;
import tiseddev.com.norsetest.databinding.ItemGmailThreadMessageBinding;
import tiseddev.com.norsetest.models.GmailThreadMessageModel;

/**
 * Created by dmytro_vodnik on 2/13/16.
 */
public class GmailThreadMessagesAdapter extends RecyclerView.Adapter<GmailThreadMessagesAdapter.ViewHolder> {

    private static final String TAG = "GMAIL THREADS MES ADAP";
    private final Context context;
    LayoutInflater layoutInflater;
    ArrayList<GmailThreadMessageModel> gmailThreadModels;

    public GmailThreadMessagesAdapter(Context context) {

        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        gmailThreadModels = new ArrayList<>();
    }

    public void addItems(List<GmailThreadMessageModel> list) {

        this.gmailThreadModels.clear();
        this.gmailThreadModels.addAll(list);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ItemGmailThreadMessageBinding binding;
        ExpandableTextView expandableTextView;

        public ViewHolder(ItemGmailThreadMessageBinding itemGmailThreadMessageBinding) {
            super(itemGmailThreadMessageBinding.getRoot());

            this.binding = itemGmailThreadMessageBinding;
            this.expandableTextView = (ExpandableTextView) this.binding.getRoot().findViewById(R.id.expand_text_view);
        }
    }

    public void addItem(GmailThreadMessageModel gmailThreadModel) {

        gmailThreadModels.add(gmailThreadModel);
        notifyItemInserted(gmailThreadModels.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        final ItemGmailThreadMessageBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.item_gmail_thread_message, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        GmailThreadMessageModel gmailThreadMessageModel = getItem(position);

        holder.binding.setGmailMessage(gmailThreadMessageModel);
        holder.binding.expandTextView.setText(gmailThreadMessageModel.getMessage());
    }

    public GmailThreadMessageModel getItem(int position) {

        return gmailThreadModels.get(position);
    }

    @Override
    public int getItemCount() {

        return gmailThreadModels.size();
    }

}