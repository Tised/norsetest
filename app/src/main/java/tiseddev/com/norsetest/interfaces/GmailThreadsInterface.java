package tiseddev.com.norsetest.interfaces;

import java.util.List;

import tiseddev.com.norsetest.models.GmailThreadModel;

/**
 * Created by dmytro_vodnik on 2/13/16.
 */
public interface GmailThreadsInterface {

    void addGmailThreads(List<GmailThreadModel> list);
    void addGmailThread(GmailThreadModel gmailThreadModel);
    List<GmailThreadModel> getAllGmailThreads();
    void setPageToken(String token);
    String getCurrentPageToken();
}
