package tiseddev.com.norsetest.interfaces;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

/**
 * Created by dmytro_vodnik on 2/13/16.
 */
public interface CredentialsInterface {

    GoogleAccountCredential getCurrentCredentials();
}
