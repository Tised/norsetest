package tiseddev.com.norsetest.interfaces;

import android.view.View;

/**
 * Created by dmytro_vodnik on 2/13/16.
 */
public interface LoadingInterface {

    void startLoading(String message);
    void stopLoading(String message);
    void errorLoading(String message, View.OnClickListener errorListener);
}
