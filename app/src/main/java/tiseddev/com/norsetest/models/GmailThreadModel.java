package tiseddev.com.norsetest.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by dmytro_vodnik on 2/13/16.
 */
@DatabaseTable
public class GmailThreadModel implements Serializable {

    @DatabaseField(generatedId = true, columnName = "id")
    int id;

    @DatabaseField
    String threadName;

    @DatabaseField
    String threadId;

    @DatabaseField
    String users;

    public GmailThreadModel(String threadName, String threadId) {
        this.threadName = threadName;
        this.threadId = threadId;
    }

    public GmailThreadModel() {}

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }

    public String getThreadName() {
        return threadName;
    }

    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    @Override
    public String toString() {
        return "GmailThreadModel{" +
                "id=" + id +
                ", threadName='" + threadName + '\'' +
                ", threadid='" + threadId + '\'' +
                '}';
    }
}
