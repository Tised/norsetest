package tiseddev.com.norsetest.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dmytro_vodnik on 2/13/16.
 */
@DatabaseTable
public class GmailThreadMessageModel {

    public static final String THREAD_ID_FIELD = "threadid";
    @DatabaseField(generatedId = true, columnName = "id")
    int id;

    @DatabaseField
    String sender;

    @DatabaseField
    String message;

    @DatabaseField
    String threadid;

    @DatabaseField
    String messageId;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getThreadid() {
        return threadid;
    }

    public void setThreadid(String threadid) {
        this.threadid = threadid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "GmailThreadMessageModel{" +
                "id=" + id +
                ", sender='" + sender + '\'' +
                ", message='" + message + '\'' +
                ", threadid='" + threadid + '\'' +
                '}';
    }
}
