package tiseddev.com.norsetest;

import android.app.Application;
import android.util.Log;

import tiseddev.com.norsetest.utils.DBUtils.HelperFactory;

/**
 * Created by dmytro_vodnik on 2/13/16.
 */
public class NorseTest extends Application {

    private static final String APP_TAG = "NORSE TEST";

    @Override
    public void onCreate(){
        super.onCreate();

        Log.d(APP_TAG, "app started");
        HelperFactory.setHelper(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }
}
