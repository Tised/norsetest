package tiseddev.com.norsetest.api_calls;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePartHeader;
import com.google.api.services.gmail.model.Thread;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import tiseddev.com.norsetest.R;
import tiseddev.com.norsetest.interfaces.GmailThreadsInterface;
import tiseddev.com.norsetest.models.GmailThreadMessageModel;
import tiseddev.com.norsetest.models.GmailThreadModel;
import tiseddev.com.norsetest.utils.Const;
import tiseddev.com.norsetest.utils.DBUtils.HelperFactory;
import tiseddev.com.norsetest.utils.DeviceStateUtils;
import tiseddev.com.norsetest.utils.SharedPrefsUtils;

/**
 * Created by dmytro_vodnik on 2/13/16.
 */
public class GetCurrentThreadTask extends AsyncTask<Void, Void, Thread> {

    private static final String TAG = "CURR THREADS TASK";
    private Gmail mService = null;
    Context context;
    private Exception mLastError;
    GoogleAccountCredential credential;
    GmailThreadsInterface gmailThreadsInterface;
    GmailThreadModel lookingThread;

    public GetCurrentThreadTask(GoogleAccountCredential credential, Context context,
                                GmailThreadsInterface gmailThreadsInterface, GmailThreadModel gmailThreadModel) {

        this.context = context;
        this.credential = credential;
        this.gmailThreadsInterface = gmailThreadsInterface;
        this.lookingThread = gmailThreadModel;
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        mService = new Gmail.Builder(
                transport, jsonFactory, credential)
                .setApplicationName(context.getString(R.string.google_api_app_name))
                .build();
    }

    @Override
    protected Thread doInBackground(Void... params) {

        try {
            return processThread(mService,
                    SharedPrefsUtils.getStringPreference(context, Const.PREF_ACCOUNT_NAME), lookingThread.getThreadId());
        } catch (Exception e) {
            cancel(true);
            mLastError = e;
            return null;
        }
    }

    private Thread processThread(Gmail mService, String userId, String threadId) throws IOException {

        return mService.users().threads().get(userId, threadId).execute();
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected void onPostExecute(Thread currentThread) {

        Log.d(TAG, "result thread === " + currentThread);

        String threadPeople = "";

        List<Message> messagesList = currentThread.getMessages();

            for (int i = 0; i < messagesList.size(); i++) {

                Message message = messagesList.get(i);

                String sender = "";

                List<MessagePartHeader> messagePartHeaders = message.getPayload().getHeaders();

                for (MessagePartHeader messagePartHeader : messagePartHeaders) {

                    if (messagePartHeader.getName().equals("To")) {

                        threadPeople = messagePartHeader.getValue();
                    }

                    if (messagePartHeader.getName().equals("Sender") || messagePartHeader.getName().equals("From")) {

                        sender = messagePartHeader.getValue();

                        threadPeople += sender;
                    }
                }

                if (i < 10) {
                    final String finalSender = sender;
                    new java.lang.Thread(() -> {

                        try {

                            GmailThreadMessageModel gmailThreadMessageModel = new GmailThreadMessageModel();

                            gmailThreadMessageModel.setMessage(message.getSnippet());
                            gmailThreadMessageModel.setSender(finalSender);
                            gmailThreadMessageModel.setThreadid(lookingThread.getThreadId());
                            gmailThreadMessageModel.setMessageId(message.getId());

                            HelperFactory.getHelper().getGmailThreadMessageDAO().create(gmailThreadMessageModel);
                        } catch (SQLException e) {

                            Log.e(TAG, "error while get message === " + Log.getStackTraceString(e));
                        }
                    }).start();
                }
            }

        try {

            lookingThread.setUsers(threadPeople);
            Log.d(TAG, "caching thread === " + lookingThread);
            HelperFactory.getHelper().getGmailThreadDAO().create(lookingThread);
            Log.d(TAG, "thread cached");

            gmailThreadsInterface.addGmailThread(lookingThread);
        } catch (SQLException e) {
            Log.e(TAG, "error while caching === " + Log.getStackTraceString(e));
        }

    }

    @Override
    protected void onCancelled() {

        if (mLastError != null) {
            if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                DeviceStateUtils.showGooglePlayServicesAvailabilityErrorDialog(
                        ((GooglePlayServicesAvailabilityIOException) mLastError)
                                .getConnectionStatusCode(), context);
            } else if (mLastError instanceof UserRecoverableAuthIOException) {
                ((Activity) context).startActivityForResult(
                        ((UserRecoverableAuthIOException) mLastError).getIntent(),
                        Const.REQUEST_AUTHORIZATION);
            } else {
                Log.d(TAG, "The following error occurred:\n"
                        + mLastError.getMessage());
            }
        } else {
            Log.d(TAG, "Request cancelled.");
        }
    }
}
