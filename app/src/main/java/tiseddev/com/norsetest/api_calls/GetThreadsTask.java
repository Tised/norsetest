package tiseddev.com.norsetest.api_calls;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListThreadsResponse;
import com.google.api.services.gmail.model.Thread;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import tiseddev.com.norsetest.R;
import tiseddev.com.norsetest.interfaces.GmailThreadsInterface;
import tiseddev.com.norsetest.interfaces.LoadingInterface;
import tiseddev.com.norsetest.models.GmailThreadModel;
import tiseddev.com.norsetest.utils.Const;
import tiseddev.com.norsetest.utils.DeviceStateUtils;
import tiseddev.com.norsetest.utils.SharedPrefsUtils;

/**
 * Created by dmytro_vodnik on 2/13/16.
 */
public class GetThreadsTask extends AsyncTask<String, Void, List<GmailThreadModel>> {

    private static final String TAG = "THREADS TASK";
    private Gmail mService = null;
    Context context;
    LoadingInterface loadingInterface;
    private Exception mLastError;
    GoogleAccountCredential credential;
    GmailThreadsInterface gmailThreadsInterface;

    public GetThreadsTask(GoogleAccountCredential credential, Context context,
                          LoadingInterface loadingInterface, GmailThreadsInterface gmailThreadsInterface) {

        this.context = context;
        this.loadingInterface = loadingInterface;
        this.credential = credential;
        this.gmailThreadsInterface = gmailThreadsInterface;
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        mService = new Gmail.Builder(
                transport, jsonFactory, credential)
                .setApplicationName(context.getString(R.string.google_api_app_name))
                .build();
    }

    @Override
    protected List<GmailThreadModel> doInBackground(String... params) {

        String pageToken = params[0];
        try {
            return listAllThreads(mService, SharedPrefsUtils.getStringPreference(context, Const.PREF_ACCOUNT_NAME), pageToken);
        } catch (Exception e) {
            cancel(true);
            mLastError = e;
            return null;
        }
    }

    @Override
    protected void onPreExecute() {

        loadingInterface.startLoading(context.getString(R.string.threads_load_started));
    }

    @Override
    protected void onPostExecute(List<GmailThreadModel> output) {

        loadingInterface.stopLoading(context.getString(R.string.success_threads_load));

        Log.d(TAG, "result threads === " + output);
    }

    @Override
    protected void onCancelled() {

        if (mLastError != null) {
            if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                DeviceStateUtils.showGooglePlayServicesAvailabilityErrorDialog(
                        ((GooglePlayServicesAvailabilityIOException) mLastError)
                                .getConnectionStatusCode(), context);
            } else if (mLastError instanceof UserRecoverableAuthIOException) {
                ((Activity) context).startActivityForResult(
                        ((UserRecoverableAuthIOException) mLastError).getIntent(),
                        Const.REQUEST_AUTHORIZATION);
            } else {
                loadingInterface.errorLoading("The following error occurred:\n"
                        + mLastError.getMessage(), v -> {

                            Log.d(TAG, "reload threads here");
                            new GetThreadsTask(credential, context, loadingInterface, gmailThreadsInterface).execute();
                        });
            }
        } else {
            loadingInterface.startLoading("Request cancelled.");
        }
    }

    String[] labelIds = {"INBOX"};

    public List<GmailThreadModel> listAllThreads(Gmail mService, String userId, String pageToken) throws IOException {

        ListThreadsResponse response = mService.users().threads().list(userId)
                .setLabelIds(Arrays.asList(labelIds))
                .setPageToken(pageToken)
                .setMaxResults(10L).execute();

        if (response == null || response.isEmpty())
            return null;

        gmailThreadsInterface.setPageToken(response.getNextPageToken());

        List<com.google.api.services.gmail.model.Thread> threadsArrayList = new ArrayList<>();
        List<GmailThreadModel> resultThreads = new ArrayList<>();
        threadsArrayList.addAll(response.getThreads());

        for (int i = 0; i < threadsArrayList.size(); i++) {

            Thread currentThread = threadsArrayList.get(i);

            GmailThreadModel newThread = new GmailThreadModel("Thread Topic", currentThread.getId());

            new GetCurrentThreadTask(credential, context, gmailThreadsInterface, newThread).execute();

            resultThreads.add(newThread);
        }

        return resultThreads;
    }
}
