package tiseddev.com.norsetest.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import tiseddev.com.norsetest.models.GmailThreadMessageModel;
import tiseddev.com.norsetest.utils.DBUtils.HelperFactory;

/**
 * Created by dmytro_vodnik on 2/13/16.
 */
public class GmailThreadMessageDAO extends BaseDaoImpl<GmailThreadMessageModel, String> {

    private static final String TAG = "GMAIL THREAD MES DAO";

    public GmailThreadMessageDAO(ConnectionSource connectionSource,
                                 Class<GmailThreadMessageModel> dataClass) throws SQLException {
        super(connectionSource, dataClass);

    }

    public List<GmailThreadMessageModel> getAllThreadMessages(String threadId) throws SQLException {
        QueryBuilder<GmailThreadMessageModel, String> queryBuilder =
                HelperFactory.getHelper().getGmailThreadMessageDAO().queryBuilder();

        Where<GmailThreadMessageModel, String> where = queryBuilder.where();
        SelectArg threadIdField = new SelectArg();

        where.eq(GmailThreadMessageModel.THREAD_ID_FIELD, threadIdField);

        PreparedQuery<GmailThreadMessageModel> preparedQuery = queryBuilder.prepare();

        threadIdField.setValue(threadId);

        return HelperFactory.getHelper().getGmailThreadMessageDAO().query(preparedQuery);
    }
}