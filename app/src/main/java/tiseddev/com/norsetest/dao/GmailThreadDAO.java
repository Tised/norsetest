package tiseddev.com.norsetest.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import tiseddev.com.norsetest.models.GmailThreadModel;

/**
 * Created by dmytro_vodnik on 2/13/16.
 */
public class GmailThreadDAO extends BaseDaoImpl<GmailThreadModel, String> {

    private static final String TAG = "GMAIL THREAD DAO";

    public GmailThreadDAO(ConnectionSource connectionSource,
                          Class<GmailThreadModel> dataClass) throws SQLException {
        super(connectionSource, dataClass);

    }

    public List<GmailThreadModel> getAllThreads() throws SQLException {

        return this.queryForAll();
    }
}